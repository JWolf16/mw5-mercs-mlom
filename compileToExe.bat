@echo off
rmdir /Q /S Compiled
rmdir /Q /S dist
py -3.7 -m PyInstaller lots.spec
rmdir /Q /S build
copy /B /Y dist\MW5-LOTS.exe .
rmdir /Q /S dist