from PySide2.QtCore import QObject, Signal, Slot
import time
import os
import os.path
import shutil
import traceback
import json
from zipfile import ZipFile, ZIP_DEFLATED, ZIP_LZMA

from .data import ModList, ModEntry, Mod, LoadOrder, LoadEntry, LotsSettings


class BkProcessor(QObject):

    WriteLoadOrder = Signal(str, bool)
    WriteOrder = Signal(str)
    ScanMods = Signal(str, str, bool)
    ResetLoadOrder = Signal()

    BkUpdate = Signal(str)
    WriteLoadOrdersComplete = Signal(bool)
    WriteOrderComplete = Signal(bool)
    ScanModsComplete = Signal(bool, str)
    ResetLOComplete = Signal(bool)

    apiVersion = 1

    def __init__(self, settings, logger, parent=None):
        """

        :param settings:
        :type settings: LotsSettings
        :param logger:
        :type logger: logging.Logger
        :param parent:
        :type parent:
        """
        super(BkProcessor, self).__init__(parent)
        self.Logger = logger
        self.debugFlag = False
        self.Settings = settings # type: LotsSettings

        self.ScanMods.connect(self._scanMods)
        self.WriteLoadOrder.connect(self._applyLoadOrder)
        self.ResetLoadOrder.connect(self._resetLoadOrder)
        self.modList = ModList()
        self.mods = {} # type: dict[str, Mod]

        self.presets = {} # type: dict[str, LoadOrder]
        self.currentLoadOrder = LoadOrder()
        if not os.path.exists(self.Settings.customLosCache):
            os.makedirs(self.Settings.customLosCache)
        self.initComplete = False

    def _scanMod(self, path, modName):
        if not os.path.exists(path):
            return False
        modJsonFile = os.path.join(path, 'mod.json')
        if os.path.exists(modJsonFile):
            mod = Mod()
            mod.readFromFile(modJsonFile, modName)
            self.Logger.info(f'Read mod.json for: {mod.displayName}-{mod.version}')
            self.mods[mod.displayName] = mod
            if mod.hasLoadOrder:
                for preset in mod.presets:
                    self.Logger.info(f'Mod Has Load Order: {preset.name}')
                    self.presets[preset.name] = preset
        return True

    def _scanModsDirectory(self, path, bRequiresModList):
        self.Logger.info(f'Scanning: {path} for mods')
        if bRequiresModList:
            modlistFile = os.path.join(path, 'modlist.json')
            if not os.path.isfile(modlistFile):
                self.Logger.error('Failed to find modlist.json')
            else:
                self.modList.readFromFile(modlistFile)
        for item in os.listdir(path):
            fpath = os.path.join(path, item)
            if os.path.isdir(fpath):
                self.Logger.info(f"Processing Mod: {item}")
                self.BkUpdate.emit(f"Scanning Mod: {item}")
                self._scanMod(fpath, item)

    def _scanMods(self, path, workShopPath, bUseWorkShop):
        for preset in os.listdir(self.Settings.customLosCache):
            if preset.endswith('.json'):
                lOrder = LoadOrder()
                if lOrder.fromFile(os.path.join(self.Settings.customLosCache, preset)):
                    lOrder.setPath(os.path.join(self.Settings.customLosCache, preset))
                    self.presets[lOrder.name] = lOrder
                    self.Logger.info(f"loaded Custom Load Order: {lOrder.name}")
        self.mods = {} # type: dict[str, Mod]
        try:

            self._scanModsDirectory(path, True)
            if bUseWorkShop:
                self._scanModsDirectory(workShopPath, False)

            if not self.modList.activePreset in self.presets:
                lOrder = LoadOrder()
                lOrder.isPreset = True
                lOrder.lotsApiVersion = self.apiVersion
                lOrder.name = self.modList.activePreset
                for mod in self.modList.mods:
                    if mod.enabled:
                        lEntry = LoadEntry()
                        lEntry.modName = mod.modName
                        lEntry.enabled = True
                        lEntry.loadOrder = 0
                        if mod in self.mods:
                            lEntry.loadOrder = self.mods[mod].loadOrder
                        lOrder.entries.append(lEntry)
                self.presets[lOrder.name] = lOrder
            self.currentLoadOrder = self.modList.activePreset
            self.Logger.info(f'CurrentLoadOrder is: {self.currentLoadOrder}')
            self.initComplete = True
            for preset in self.presets:
                if self.presets[preset].lotsApiVersion > self.apiVersion:
                    self.ScanModsComplete.emit(False, f"higher L.O.T.S API version detected in load order than this application supports, please upgrade to a newer version that supports at least api v{self.presets[preset].lotsApiVersion}")
                    return

            self.Logger.error('Mod Scan Complete')
            self.ScanModsComplete.emit(True, "Mod scan successful")

        except:
            trace = traceback.format_exc().split('\n')
            self.Logger.critical('Unhandled mod scan exception!')
            for line in trace:
                self.Logger.critical(line)
            self.ScanModsComplete.emit(False, "Mod Scan encountered a fatal error, please consider reporting this to Jamie Wolf")

    def _applyLoadOrder(self, stLoadOrder, bDisableUnused):
        try:
            activeMods = []

            for mod in self.modList.mods:
                if mod.enabled:
                    activeMods.append(mod.modName)
            loadOrder = self.presets[stLoadOrder]
            self.Logger.info(f"Applying Load Order: {loadOrder.name}")
            modlist = ModList()
            modlist.activePreset = loadOrder.name
            modlist.gameVersion = self.Settings.lastGameVersion
            for mod in self.mods:
                self.Logger.info(f"Processing Mod: {mod}")
                modObj = self.mods[mod]
                entry = loadOrder.getEntry(modObj.displayName)
                if entry is None:
                    self.Logger.info('looking for fallback of dir name...')
                    entry = loadOrder.getEntry(modObj.name)
                bEnab = False
                if entry:
                    if entry.enabled:
                        bEnab = True
                    modObj.loadOrder = entry.loadOrder
                    modObj.writeToFile()
                    self.Logger.info(f"updating load order to: {entry.loadOrder}")
                else:
                    self.Logger.info('entry not found, assuming its disabled/missing')
                self.Logger.info(f"setting enabled to: {bEnab}")
                modEntry = ModEntry()
                modEntry.modName = modObj.name
                modEntry.enabled = bEnab
                # in this case leave it enabled
                if not bDisableUnused and not bEnab:
                    if modEntry.modName in activeMods:
                        self.Logger.info("overwriting load order based on user selection")
                        modEntry.enabled = True
                modlist.mods.append(modEntry)
            self.modList = modlist
            self.currentLoadOrder = loadOrder.name
            self.modList.writeToFile(os.path.join(self.Settings.modsDirectory, 'modlist.json'))
            self.Logger.info("wrote modlist.json")
            self.WriteLoadOrdersComplete.emit(True)
        except:
            trace = traceback.format_exc().split('\n')
            self.Logger.critical('Unhandled mod load order write exception!')
            for line in trace:
                self.Logger.critical(line)
            self.WriteLoadOrdersComplete.emit(False)

    def _resetLoadOrder(self):
        try:
            for mod in self.mods:
                self.Logger.info(f"Processing Mod: {mod}")
                modObj = self.mods[mod]
                modObj.resetLO()
                modObj.writeToFile()
                self.Logger.info(f"updating load order to: {modObj.originalLO}")
            self.ResetLOComplete.emit(True)
        except:
            trace = traceback.format_exc().split('\n')
            self.Logger.critical('Unhandled mod load order reset exception!')
            for line in trace:
                self.Logger.critical(line)
            self.ResetLOComplete.emit(False)

    def _interrogate(self, stPath):
        self.Logger.info('Starting Interrogation...')
        try:
            interrogator = ZipFile(stPath, 'w', compression=ZIP_DEFLATED)
            if os.path.isfile('LOTS_Log.txt'):
                interrogator.write('LOTS_Log.txt', arcname='LOTS_Log.txt')
            if os.path.isfile('Mw5LotsSettings.json'):
                interrogator.write('Mw5LotsSettings.json', arcname='Mw5LotsSettings.json')
            interrogator.close()
            self.InterrogationComplete.emit(True)
        except Exception as e:
            self.Logger.error("Failed to Interrogate!")
            trace = traceback.format_exc().split('\n')
            for line in trace:
                self.Logger.error(line)
            self.InterrogationComplete.emit(False)
        self.Logger.info('Interrogation Complete')
