from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *
from ..data import ETabs
from ..bkprocessor import BkProcessor
import logging


class BaseTabWidget(QWidget):

    Id = ETabs.BaseTab
    GenOpFailWarning = True

    def __init__(self, bkprocessor, logger,  parent=None):
        """

        :param bkprocessor:
        :type bkprocessor: BkProcessor
        :param parent:
        :type parent:
        """
        super(BaseTabWidget, self).__init__(parent)
        self.logger = logger  # type: logging.Logger
        self.bkProcessor = bkprocessor  # type: BkProcessor
        self.ProgressDialog = None

    def setupProgressDialog(self):
        self.ProgressDialog = QProgressDialog('Operation In Progress...', 'Cancel', 0, 0, self)
        self.ProgressDialog.setWindowFlags(self.ProgressDialog.windowFlags() & ~Qt.WindowContextHelpButtonHint)
        self.ProgressDialog.setLabelText('Operation In Progress...')
        self.ProgressDialog.setCancelButton(None)
        self.ProgressDialog.setWindowTitle('Operation In Progress...')
        self.ProgressDialog.setWindowModality(Qt.WindowModal)
        self.ProgressDialog.showNormal()

    def genericOpFinished(self, bSuccess):
        if self.ProgressDialog is not None:
            self.ProgressDialog.reset()
            self.genericCallback()
        if not bSuccess:
            self.genericFailCallback()
            if self.GenOpFailWarning:
                QMessageBox.warning(self, 'Operation Failed!', "Operation has Failed")

    def genericCallback(self):
        pass

    def genericFailCallback(self):
        pass

    def reload(self):
        pass