import json
import io
import uuid
from .basejsonobject import BaseDataObject


class ModEntry:

    def __init__(self, name=None, enabled=None):
        self.modName = ""
        self.enabled = False
        if name is not None:
            self.modName = name
        if enabled is not None:
            self.enabled = enabled


class ModList:

    activeKey = "LOTSActivePreset"
    statusKey = "modStatus"
    gameVersionKey = "gameVersion"

    def __init__(self):
        self.mods = [] #type: list[ModEntry]
        self.activePreset = ""
        self.gameVersion = ""

    def readFromFile(self, filePath):
        m_file = io.open(filePath, 'rb')
        data = m_file.read()
        data = data.decode(BaseDataObject.getUtfEncoding(data))
        m_file.close()
        jData = json.loads(data)

        self.mods = []
        if self.activeKey in jData:
            self.activePreset = jData[self.activeKey]
        else:
            self.activePreset = f"UnknownLoadOrder-{str(uuid.uuid4()).split('-')[-1]}"
        if self.gameVersionKey in jData:
            self.gameVersion = jData[self.gameVersionKey]

        if self.statusKey in jData:
            for item in jData[self.statusKey].keys():
                enab = jData[self.statusKey][item]["bEnabled"]
                entry = ModEntry(item, enab)
                self.mods.append(entry)

    def writeToFile(self, filePath):
        jData = {}
        jData[self.activeKey] = self.activePreset
        jData[self.gameVersionKey] = self.gameVersion
        jData[self.statusKey] = {}
        for mod in self.mods:
            jData[self.statusKey][mod.modName] = {
                "bEnabled" : mod.enabled
            }
        strData = json.dumps(jData, indent=4, ensure_ascii=False)
        strData = strData.encode('utf-8')
        m_file = io.open(filePath, mode='wb')
        m_file.write(strData)
        m_file.flush()
        m_file.close()

        pass

