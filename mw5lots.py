from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *

# This is needed as pyinstaller will not find this import otherwise
# see: https://cffi.readthedocs.io/en/latest/cdef.html for details
import _cffi_backend

import sys
import time
import os
import os.path
import logging
import logging.handlers
import argparse
import subprocess
import traceback
import random
import certifi

from mlom import Ui_Mlom
from libmwlots import *

rootLogger = logging.getLogger('LOTS')
rootLogger.setLevel(logging.INFO)
obj_handler = logging.FileHandler('LOTS_Log.txt', mode='w')
obj_formatter = logging.Formatter('%(levelname)-10s (%(module)-20s:%(funcName)-30s:%(lineno)-4d) %(message)s')
obj_handler.setFormatter(obj_formatter)
obj_handler.setLevel(logging.DEBUG)
rootLogger.addHandler(obj_handler)

################# CERTIFI WORK_AROUND + RANT #####################

# Certifi seems either incapable or unwilling to fix this years old bug with freezing
# despite having a pull request that would potentially fix the issue for almost 2 years
# please just fix this, there is no reason why this burden should be placed on app makers
# to deal with and no good alternative library available
import pkgutil
import tempfile

# Read the cert data
cert_data = pkgutil.get_data('certifi', 'cacert.pem')

# Write the cert data to a temporary file
handle = tempfile.NamedTemporaryFile(delete=False)
handle.write(cert_data)
handle.flush()

# Set the temporary file name to an environment variable for the requests package
os.environ['REQUESTS_CA_BUNDLE'] = handle.name
rootLogger.info("Cert Path: {0}".format(handle.name))

STR_VERSION = '1.0.1'


class Mw5Lots(QMainWindow, Ui_Mlom):
    """
    Main UI

    """

    SettingsFile = 'Mw5LotsSettings.json'

    def __init__(self, obj_qapp, logger, parent=None):
        """

        :param obj_qapp:
        :type obj_qapp:
        :param logger:
        :type logger: logging.Logger
        :param parent:
        :type parent:
        """
        super(Mw5Lots, self).__init__(parent)
        self.setupUi(self)
        self.qapp = obj_qapp
        self.agentId = 'MW5 Load Order Tool Set v' + STR_VERSION
        self.setWindowTitle(self.agentId)
        self.ProgressDialog = None
        self.Logger = logger

        self.Settings = LotsSettings()
        if os.path.exists(self.SettingsFile):
            self.Settings.fromFile(self.SettingsFile)
        else:
            self.Settings.toFile(self.SettingsFile)

        self.lineEdit_gameVersion.setText(self.Settings.lastGameVersion)

        self.setSkin('Fusion')

        # Create a thread to run background/long running tasks
        self.bkThread = QThread()
        self.bkProcessor = BkProcessor(self.Settings, self.Logger)
        self.bkProcessor.moveToThread(self.bkThread)
        self.bkThread.start()

        self.bkProcessor.BkUpdate.connect(self.bkUpdate)
        self.bkProcessor.ScanModsComplete.connect(self.scanModsComplete)
        self.bkProcessor.WriteOrderComplete.connect(self.reloadTabs)

        self.pushButton_browse.pressed.connect(self.modDirSelect)
        self.pushButton_refresh.pressed.connect(self.scanMods)
        self.pushButton_browseSteam.pressed.connect(self.modWorkshopSelect)
        self.lineEdit_gameVersion.editingFinished.connect(self.updateGameVersion)
        self.groupBox_debug.setVisible(False)

        self.tabs = [PresetsTabController, CustomOrderTabController] # type: list[BaseTabWidget]

        self.menuEditors = QMenu('Tabs')
        for tab in self.tabs:
            actionInventory = QAction(self.menuEditors)
            actionInventory.setText(tab.Id)
            actionInventory.triggered.connect(self.activateEditor)
            self.menuEditors.addAction(actionInventory)
        self.menubar.addMenu(self.menuEditors)

        self.mdiArea.setViewMode(QMdiArea.TabbedView)
        self.mdiArea.setTabsClosable(True)
        self.mdiArea.setDocumentMode(True)

        initTabs = [ETabs.CustomOrder, ETabs.Presets]
        for id in initTabs:
            self.showEditor(id)

        self.scanMods()

    def scanMods(self):

        if self.Settings.modsDirectory != '':
            self.lineEdit_modLocation.setText(self.Settings.modsDirectory)
            self.lineEdit_modLocationSteam.setText(self.Settings.workshopModsDirectory)
            self.ProgressDialog = QProgressDialog('Scanning Mods', 'Cancel', 0, 0, self)
            self.ProgressDialog.setWindowFlags(self.ProgressDialog.windowFlags() & ~Qt.WindowContextHelpButtonHint)
            self.ProgressDialog.setLabelText('Scanning Mods...')
            self.ProgressDialog.setCancelButton(None)
            self.ProgressDialog.setWindowTitle('Scanning Mods')
            self.ProgressDialog.setWindowModality(Qt.WindowModal)
            self.ProgressDialog.showNormal()
            self.bkProcessor.ScanMods.emit(self.Settings.modsDirectory, self.Settings.workshopModsDirectory, self.Settings.workshopModsDirectory != '')

    def modDirSelect(self):
        str_file_info = QFileDialog.getExistingDirectory(self, caption="Select MW Mercs Mod directory",
                                                         dir=self.Settings.modsDirectory)
        if str_file_info != '':
            self.Settings.modsDirectory = str_file_info
            self.Settings.toFile(self.SettingsFile)
            self.scanMods()

    def modWorkshopSelect(self):
        str_file_info = QFileDialog.getExistingDirectory(self, caption="Select MW Mercs Steam/Epic Workshop directory",
                                                         dir=self.Settings.workshopModsDirectory)
        if str_file_info != '':
            self.Settings.workshopModsDirectory = str_file_info
            self.Settings.toFile(self.SettingsFile)
            self.scanMods()

    def bkUpdate(self, str_message):
        """
        update the progress dialog text during a background operation

        :param str_message: the message to display
        :type str_message: str
        :return:
        :rtype:
        """
        if self.ProgressDialog is not None:
            self.ProgressDialog.setLabelText(str_message)

    def reloadTabs(self):
        for window in self.mdiArea.subWindowList():
            window.widget().reload()

    def scanModsComplete(self, bsuccess, stMessage):
        if self.ProgressDialog is not None:
            self.ProgressDialog.reset()
        if bsuccess:
            self.reloadTabs()
            QMessageBox.information(self, 'Scan Success', stMessage)
        else:
            QMessageBox.critical(self, "Scan Failed!", stMessage)

    def updateGameVersion(self):
        print('Here')
        self.Settings.lastGameVersion = self.lineEdit_gameVersion.text()
        self.Settings.toFile(self.SettingsFile)


    def closeEvent(self, event):
        """
        handle shutdown gracefully

        :param event:
        :type event:
        :return:
        :rtype:
        """
        self.bkThread.terminate()
        while not self.bkThread.isFinished():
            time.sleep(0.05)
        event.accept()

    def setFusionPalette(self):
        palette = QPalette()
        palette.setColor(QPalette.Window, QColor(53, 53, 53))
        palette.setColor(QPalette.WindowText, Qt.white)
        palette.setColor(QPalette.Base, QColor(15, 15, 15))
        palette.setColor(QPalette.AlternateBase, QColor(53, 53, 53))
        palette.setColor(QPalette.ToolTipBase, Qt.white)
        palette.setColor(QPalette.ToolTipText, Qt.white)
        palette.setColor(QPalette.Text, Qt.white)
        palette.setColor(QPalette.Button, QColor(53, 53, 53))
        palette.setColor(QPalette.ButtonText, Qt.white)
        palette.setColor(QPalette.BrightText, Qt.red)
        palette.setColor(QPalette.PlaceholderText, Qt.gray)

        palette.setColor(QPalette.Highlight, QColor(142, 45, 197).lighter())
        palette.setColor(QPalette.HighlightedText, Qt.black)

        palette.setColor(QPalette.Disabled, QPalette.ButtonText, Qt.darkGray)
        palette.setColor(QPalette.Disabled, QPalette.PlaceholderText, Qt.darkGray)
        palette.setColor(QPalette.Disabled, QPalette.Text, Qt.darkGray)
        palette.setColor(QPalette.Disabled, QPalette.WindowText, Qt.darkGray)

        self.qapp.setPalette(palette)

    def updateSkin(self):
        """
        change the skin of the UI

        :return:
        :rtype:
        """
        str_skin = self.sender().text()
        self.setSkin(str_skin)

    def setSkin(self, str_skin):
        """
        apply a stylesheet operation

        :param str_skin: the skin to change to, eithr an option in self.skinStyles or a builtin one
        :type str_skin: str
        :return:
        :rtype:
        """
        # print 'Selecting Skin: {0}'.format(str_skin)
        if str_skin in QStyleFactory.keys():
            self.qapp.setStyle(QStyleFactory.create(str_skin))
            self.qapp.setStyleSheet("")
            if str_skin == 'Fusion':
                self.setFusionPalette()
            else:
                self.qapp.setPalette(self.style().standardPalette())
        else:
            self.qapp.setPalette(self.style().standardPalette())
            self.qapp.setStyle(QStyleFactory.create('windowsvista'))
            mfile = QFile(":/stylesheets/{0}".format(str_skin))
            mfile.open(QFile.ReadOnly)
            style = mfile.readAll()
            self.qapp.setStyleSheet(style.data().decode('utf8'))
            mfile.close()

    def showEditor(self, stEditorId):
        lst_windows = self.mdiArea.subWindowList()
        for window in lst_windows:
            if window.widget().Id == stEditorId:
                self.mdiArea.setActiveSubWindow(window)
                return
        for tab in self.tabs:
            if tab.Id == stEditorId:
                objTab = tab(self.bkProcessor, self.Logger)
                self.mdiArea.addSubWindow(objTab, Qt.SubWindow)
                objTab.showMaximized()
                objTab.reload()
                return


    def activateEditor(self):
        """
        activate the correct editor tabe, or spawn it if its not open

        :return:
        :rtype:
        """
        stTab = self.sender().text()
        self.showEditor(stTab)



if __name__ == '__main__':

    rootLogger.info('----- MW5 L.O.T.S v{0} -----'.format(STR_VERSION))

    QApplication.setAttribute(Qt.AA_EnableHighDpiScaling, True)

    obj_main_app = QApplication(sys.argv)
    obj_main_window = Mw5Lots(obj_main_app, rootLogger)
    obj_main_window.show()
    obj_main_app.exec_()